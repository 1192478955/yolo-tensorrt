include(cmake/pkgs.cmake)

# ### CUDA
# find_package(CUDA REQUIRED)

# list(APPEND ALL_INC_DIRS 
#     ${CUDA_INCLUDE_DIRS})
# list(APPEND ALL_LIBS 
#   ${CUDA_LIBRARIES} 
#   ${CUDA_cublas_LIBRARY} 
#   ${CUDA_nppc_LIBRARY} 
#   ${CUDA_nppig_LIBRARY} 
#   ${CUDA_nppidei_LIBRARY} 
#   ${CUDA_nppial_LIBRARY})

### TensorRT
find_library(TRT_NVINFER NAMES nvinfer HINTS ${TensorRT_ROOT} PATH_SUFFIXES lib lib64 lib/x64)
find_library(TRT_NVINFER_PLUGIN NAMES nvinfer_plugin HINTS ${TensorRT_ROOT} PATH_SUFFIXES lib lib64 lib/x64)
#find_library(TRT_NVONNX_PARSER NAMES nvonnxparser HINTS ${TensorRT_ROOT} PATH_SUFFIXES lib lib64 lib/x64)
#find_library(TRT_NVCAFFE_PARSER NAMES nvcaffe_parser HINTS ${TensorRT_ROOT} PATH_SUFFIXES lib lib64 lib/x64)
find_path(TENSORRT_INCLUDE_DIR NAMES NvInfer.h HINTS ${TensorRT_ROOT} PATH_SUFFIXES include)

message("TI : " ${TENSORRT_INCLUDE_DIR})

list(APPEND ALL_INC_DIRS ${TENSORRT_INCLUDE_DIR})
list(APPEND ALL_LIBS 
    ${TRT_NVINFER} 
    ${TRT_NVINFER_PLUGIN} 
    # ${TRT_NVONNX_PARSER} 
    # ${TRT_NVCAFFE_PARSER}
    )

set(SAMPLES_COMMON_DIR ${TensorRT_ROOT}/samples/common)
list(APPEND ALL_INC_DIRS ${SAMPLES_COMMON_DIR})
set(TRT_SAMPLES_CPPS 
  ${TensorRT_ROOT}/samples/common/logger.cpp 
  )

### OPENCV
list(APPEND CMAKE_PREFIX_PATH "${OpenCV_DIR}/x64/vc16/lib")
find_package(OpenCV 4 REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})