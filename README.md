# Yolo-TensorRT

#### 介绍
基于 CUDA与TensorRT的YOLO，
其中包括YOLO v8.

#### 软件架构
后续可继续添加其他版本 YOLO V8支持.


#### 安装教程
1. 用 vs 以 cmake方式导入
2. 用 vs code
3. 用命令行

* 需要在 `cmake/pkgs.cmake` 中修改目录位置
* 需要修改 `tests/app_yolov8.cpp` 中的模型文件位置与测试视频位置
* debug编译后, 需将`data`复制到 `build/tests/debug/`中（默认从其中读模型文件与测试视频）
* 复制动态库 `opencv_world480d.dll` 和 `opencv_videoio_ffmpeg480_64.dll`等 到 `app_yolov8.exe` 所在目录(`build/tests/debug/`)
