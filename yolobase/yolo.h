#pragma once

#include "kernel_function.h"
#include "types.h"
#include <NvInfer.h>

namespace yolo
{

    class YOLO
    {
    public:
        YOLO(const YoloParams& param);
        ~YOLO();

    public:
        virtual bool init(const std::string& mdl_file);
        virtual std::vector<std::vector<Box>> detect(const std::vector<cv::Mat>& imgsBatch) final;

    protected:
        virtual int load_model(const std::string& mdl_file) final;
        virtual void check() final;
        
        virtual void copy(const std::vector<cv::Mat>& imgsBatch) final;

        virtual void preprocess(const std::vector<cv::Mat>& imgsBatch);
        virtual std::vector<std::vector<Box>> postprocess(const std::vector<cv::Mat>& imgsBatch);

        virtual bool infer() final;
        virtual void reset() final;

    private:
        virtual int load_model(const std::string& mdl_file, std::vector<uint8_t>& data) final;

    protected:
        std::shared_ptr<nvinfer1::ICudaEngine> m_engine;
        std::unique_ptr<nvinfer1::IExecutionContext> m_context;

    protected:
        YoloParams m_param;
        nvinfer1::Dims m_output_dims;   // (1, 25200, 85, 0, 0...)
        int m_output_area;              // 1 * 25200 * 85
        int m_total_objects;            // 25200

        // (m_param.dst_h, m_param.dst_w) to (m_param.src_h, m_param.src_w) 
        AffineMat m_dst2src;     // 2*3

        // input
        //float* m_input_src_device;
        unsigned char* m_input_src_device;
        float* m_input_resize_device;
        float* m_input_rgb_device;
        float* m_input_norm_device;
        float* m_input_hwc_device;
        //float* m_input_dst_device;

        // output
        float* m_output_src_device;     // malloc in init()
        float* m_output_objects_device;
        float* m_output_objects_host;
        int m_output_objects_width;     // 7:left, top, right, bottom, confidence, class, keepflag; 
        int* m_output_idx_device;       // nmsv2
        float* m_output_conf_device;
    };
}
