#pragma once

#include "types.h"
#include <opencv2/opencv.hpp>

namespace utils 
{
    /************************************************************************************************
    * function
    *************************************************************************************************/
    void show(const std::vector<std::vector<yolo::Box>>& objectss, const std::vector<std::string>& classNames,
        const int& cvDelayTime, std::vector<cv::Mat>& imgsBatch);

    void save(const std::vector<std::vector<yolo::Box>>& objectss, const std::vector<std::string>& classNames,
        const std::string& savePath, std::vector<cv::Mat>& imgsBatch, const int& batchSize, const int& batchi);
}
