#pragma once

#include <string>
#include <vector>
#include <opencv2/core/core.hpp>

namespace yolo
{
    struct YoloParams
    {
        int num_class{ 80 }; // coco 
        std::vector<std::string> class_names;
        std::vector<std::string> input_output_names;
        std::string model_file;

        bool dynamic_batch{ true };
        int batch_size{0};
        int src_h{0};
        int src_w{0}; // size of source image eg:unknow * unknow
        int dst_h{0};
        int dst_w{0}; // size of net's input, eg:640*640

        float scale{ 255.f };
        float means[3] = { 0.f, 0.f, 0.f };
        float stds[3] = { 1.f, 1.f, 1.f };

        float iou_thresh;
        float conf_thresh;

        int topK{ 1000 };
    };

    struct Box
    {
        float left, top, right, bottom, confidence;
        int label;
        std::vector<cv::Point2i> land_marks;

        Box() = default;
        Box(float left, float top, float right, float bottom, float confidence, int label) :
            left(left), top(top), right(right), bottom(bottom), confidence(confidence), label(label) {}

        Box(float left, float top, float right, float bottom, float confidence, int label, int numLandMarks) :
            left(left), top(top), right(right), bottom(bottom), confidence(confidence), label(label)
        {
            land_marks.reserve(numLandMarks);
        }
    };


    enum class ColorMode { RGB, GRAY };

    struct AffineMat
    {
        float v0, v1, v2;
        float v3, v4, v5;
    };
}



