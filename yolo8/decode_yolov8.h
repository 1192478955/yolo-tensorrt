#pragma once

#include "yolobase/kernel_function.h"
#include "yolobase/types.h"

namespace yolo
{
	void decodeDevice(const YoloParams& param, float* src, int srcWidth, int srcHeight, int srcLength, float* dst, int dstWidth, int dstHeight);
	void transposeDevice(const YoloParams& param, float* src, int srcWidth, int srcHeight, int srcArea, float* dst, int dstWidth, int dstHeight);
}
