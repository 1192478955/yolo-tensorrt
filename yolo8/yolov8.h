#pragma once
#include "yolobase/yolo.h"

namespace yolo
{
	class YOLOV8 : public YOLO
	{
	public:
		YOLOV8(const YoloParams& param);
		~YOLOV8();

	public:
		virtual bool init(const std::string& mdl_file) override;

	protected:
		virtual void preprocess(const std::vector<cv::Mat>& imgsBatch) override;
		virtual std::vector<std::vector<Box>> postprocess(const std::vector<cv::Mat>& imgsBatch) override;

	private:
		float* m_output_src_transpose_device;
	};
}

